import os
import sys

paths = ['/Users/bettse/Projects/pygl', '/Users/bettse/Projects/pygl/mysite/myapp', '/Users/bettse/Projects/pygl/mysite', '/lib/python2.6/site-packages/']
for path in paths:
    if path not in sys.path:
        sys.path.append(path)

os.environ['DJANGO_SETTINGS_MODULE'] = 'mysite.settings'
os.environ['HOME']='/tmp'

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

