from django.conf.urls.defaults import patterns, include, url
from django.conf import settings
from mysite.myapp.charts import *
from mysite.myapp.views import *
from mysite.myapp import json

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'mysite.views.home', name='home'),
    # url(r'^mysite/', include('mysite.foo.urls')),


    #Regular pages
    (r'^$', extjs),
    (r'^debug$', debugextjs),

    #iCal files
    (r'^ical/(?P<account>[\w:\s]+)/events.ics$', xacts_cal),
    (r'^ical/(?P<account>[\w:\s]+)/total.ics$', total_cal),

    #Extjs json urls
    (r'^json/budget$', 'mysite.myapp.json.budget'),
    (r'^json/assets$', 'mysite.myapp.json.assets'),
    (r'^json/accounts$', 'mysite.myapp.json.accounts'),
    (r'^json/unbudgeted$', 'mysite.myapp.json.unbudgeted'),
    (r'^json/eomestimate$', 'mysite.myapp.json.eomestimate'),
    (r'^json/balance/year/(?P<account>[\w:\s]+)$', 'mysite.myapp.json.yearbalance'),
    (r'^json/balance/$', 'mysite.myapp.json.newbalance'),
    (r'^json/balance/month/(?P<account>[\w:\s]+)$', 'mysite.myapp.json.monthbalance'),
    (r'^json/balance/month/$', 'mysite.myapp.json.monthbalance'),
    (r'^json/spending/year/(?P<account>[\w:\s]+)$', 'mysite.myapp.json.yearspending'),
    (r'^json/activity/recent/(?P<account>[\w:\s]+)$', 'mysite.myapp.json.recentactivity'),
    (r'^json/activity/forecast/(?P<account>[\w:\s]+)$', 'mysite.myapp.json.forecastactivity'),
    (r'^json/forecast/(?P<account>[\w:\s]+)$', 'mysite.myapp.json.forecast'),


    #static content
    (r'^site_media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT})

)
