from django.http import HttpResponse
from ledger import *
# do this before importing pylab or pyplot
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.dates import DateFormatter
import matplotlib.dates as mdates
import matplotlib.mlab as mlab
import matplotlib.cbook as cbook
import matplotlib.ticker as ticker

from pylab import *

import datetime
import numpy as np
from pprint import pprint

import locale
locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')

def piechart(request):
    parameters = ['balance', 'Expenses', '-E', '-p', 'this month','--flat', '--no-total', '-F', '%(account)\t%(amount)\n']
    output = runledger(parameters)

    labels = []
    values = []
    for line in output.split('\n'):
        if(line == ""):
            continue
        label, value = line.split('\t')
        values.append(float(sanatize(value)))
        labels.append(prettyname(label))

    fig = figure(figsize=(8,8), frameon=False)
    ax = axes([0.1, 0.1, 0.8, 0.8])

    pie(values, explode=[0.1 for x in labels], labels=labels, autopct='%1.1f%%', shadow=True)
    title("Breakdown of this month's spending")

    canvas=FigureCanvas(fig)
    response=HttpResponse(content_type='image/png')
    canvas.print_png(response)
    plt.close(fig)
    return response

def budgetpie(request):
    parameters = ['budget', 'Expenses', 'checking', '-p', 'last month','--flat', '--no-total', '-F', '%(account)\t%(get_at(total_expr, 1))\n']
    output = runledger(parameters)

    budget = {}
    for line in output.split('\n'):
        if(line == ""):
            continue
        label, value = line.split('\t')
        budget[prettyname(label)] = float(sanatize(value))*-1

    fig = figure(figsize=(8,8), frameon=False)
    ax = axes([0.1, 0.1, 0.8, 0.8])

    pie(budget.values(), labels=budget.keys(), autopct='%1.1f%%')
    title("Monthly Budget")

    canvas=FigureCanvas(fig)
    response=HttpResponse(content_type='image/png')
    canvas.print_png(response)
    plt.close(fig)
    return response


def plot_next_x_days(request, account, days):
    return plotdays(request, account, 'next', days)

def plot_last_x_days(request, account, days):
    return plotdays(request, account, 'last', days)

def plotdays(request, account, dir, days):
    base_parameters = ['-E', '--sort', 'd', '--weekly', '-J']
    if(dir == "next"):
        title = "Forecast of " + prettyname(account) + " for next " + days + " days "
        return plot(request, base_parameters + ['--forecast', 'd>[today] & d<[today]+'+days, '-d', 'd>[today] & d<[today]+'+days, 'register'] + ["^" + account], title)
    elif (dir == "last"):
        title = "Last " + days + " days of " + prettyname(account)
        return plot(request, base_parameters + ['-d', 'd<[today] & d>[today]-'+days, 'register'] + ["^" + account], title)



def plot_daily_balance(request, account):
    base_parameters = ['--sort', 'd', '--daily', '-J', '-p', 'daily', '-d', 'd>=[this month] & d <= [today]', 'register'] + ["^" + account]
    return plot(request, base_parameters, "This month daily balance")

def plot(request, parameters = [], title=""):

    years    = mdates.YearLocator()   # every year
    months   = mdates.MonthLocator()  # every month
    quarters = mdates.MonthLocator(interval=3)  # every month
    weeks    = mdates.WeekdayLocator(byweekday=mdates.MO, interval=1)
    days     = mdates.DayLocator()    # every day

    yearsFmt = mdates.DateFormatter('%Y')
    monthsFmt = mdates.DateFormatter('%m/%Y')
    daysFmt = mdates.DateFormatter('%m/%d')


    output = runledger(parameters)
    times = []
    values = []
    for line in output.split('\n'):
        if(line == ""):
            continue
        times.append(datetime.datetime.strptime(line.split()[0], "%Y-%m-%d"))
        values.append(float(line.split()[1].replace(',', '.')))

    if(len(values) <= 0):
        values.append(0)
        times.append(datetime.datetime.today())



    fig = plt.figure()
    ax = fig.add_subplot(111)

    if(len(times) < 11):
        ax.plot_date(times, values, 'D-', xdate=True)
    elif(len(times) < 26):
        ax.plot_date(times, values, 'o-', xdate=True)
    elif(len(times) < 53):
        ax.plot_date(times, values, '-', xdate=True)
    else:
        ax.plot_date(times, values, '.', xdate=True)


    ax.set_title(title)

    daterange = (max(times) - min(times)).days
    # format the ticks
    if(daterange < 15 ):
        ax.xaxis.set_major_locator(days)
        ax.xaxis.set_major_formatter(daysFmt)
    elif(daterange < 35 ):
        ax.xaxis.set_major_locator(weeks)
        ax.xaxis.set_major_formatter(daysFmt)
    elif(daterange < 90):
        ax.xaxis.set_major_locator(weeks)
        ax.xaxis.set_major_formatter(monthsFmt)
    elif(daterange < 250):
        ax.xaxis.set_major_locator(months)
        ax.xaxis.set_major_formatter(monthsFmt)
    elif(daterange < 400):
        ax.xaxis.set_major_locator(quarters)
        ax.xaxis.set_major_formatter(monthsFmt)
    else:
        ax.xaxis.set_major_locator(years)
        ax.xaxis.set_major_formatter(yearsFmt)

    ax.yaxis.set_major_formatter(moneyFmt)

    daterange = datetime.timedelta(int(round(daterange * .05)))
    datemin = min(times) - daterange
    datemax = max(times) + daterange
    ax.set_xlim(datemin, datemax)

    ax.set_ylim(min(values + [0.0]) * 1.1, max(values + [0.0]) * 1.1)

    # format the coords message box
    ax.format_xdata = mdates.DateFormatter('%Y-%m-%d')
    ax.format_ydata = price
    ax.grid(True)

    # rotates and right aligns the x labels, and moves the bottom of the
    # axes up to make room for them
    fig.autofmt_xdate()

    canvas=FigureCanvas(fig)
    response=HttpResponse(content_type='image/png')
    canvas.print_png(response)
    plt.close(fig)
    return response

def budget_surplus(request):

    parameters = ['-w', '-F', '%(account)\t%(amount)\n', '-p', 'this month', '--flat', '--no-total', '--budget', '-M', 'reg', '^exp']

    output = runledger(parameters)
    labels = []
    values = []
    for line in output.split('\n'):
        if(line == ""):
            continue
        label, value = line.split('\t')
        label = label.split(':')[-1]
        labels.append(label)
        value = float(sanatize(value))
        values.append(value*-1)


    fig = plt.figure()
    ax = fig.add_subplot(111)

    ax.set_title('Budget surplus/shortfall this month')

    color_list = [redgreen(v) for v in values]
    ax.yaxis.set_major_formatter(moneyFmt)
    ax.set_ylim(min(values + [0.0]) * 1.1, max(values + [0.0]) * 1.1)
    ax.format_ydata = price
    ax.grid(True)

    fig.autofmt_xdate()
    x = arange(len(labels))
    bar(x, values, color=color_list)
    xticks( x + 0.5,  labels )

    canvas=FigureCanvas(fig)
    response=HttpResponse(content_type='image/png')
    canvas.print_png(response)
    plt.close(fig)

    return response

def redgreen(x):
    if x <= 0:
        return 'red'
    else:
        return 'green'

def budget_boxplot(request):

    parameters = ['-w', '-E', '-F', '%(account)\n', '-p', 'this month', '--flat', '--no-total', '--budget', '-M', 'bal', '^exp']
    data_parameters = ytd + ['-w', '-F', '%(amount)\n', '-E', '--budget', '-M', 'reg']

    output = runledger(parameters)
    accounts = [acct for acct in output.split('\n') if acct != ""]

    data = []
    labels = []
    for acct in accounts:
        output = runledger(data_parameters + ["^" + acct])
        values = []
        for value in output.split('\n'):
            if value == "":
                continue
            value = float(sanatize(value))
            values.append(value)
        data.append(values)
        labels.append(acct.split(':')[-1])

    fig = plt.figure()
    ax = fig.add_subplot(111)

    boxplot(data)

    title('Boxplot of expenses by month this year')
    ax.yaxis.set_major_formatter(moneyFmt)
    ax.format_ydata = price
    ax.grid(True)


    fig.autofmt_xdate()
    ax.set_xticklabels(labels)
    #xticks( arrange(len(labels)), labels )

    canvas=FigureCanvas(fig)
    response=HttpResponse(content_type='image/png')
    canvas.print_png(response)
    plt.close(fig)

    return response

def account_boxplot(request, account):

    if ("Expenses" in account.split(':')):
        format = "amount"
    else:
        format = "total"

    parameters = ytd + ['-F', '%(' + format + ')\n'] +  ['-w', '-M', 'reg', "^" + account]


    output = runledger(parameters)
    values = [float(sanatize(line)) for line in output.split('\n') if line != ""]
    if(len(values) <= 0): values.append(0)

    fig = plt.figure()
    ax = fig.add_subplot(111)

    boxplot(values)

    title("Boxplot of " + prettyname(account) + " " + format + " by month")

    ax.yaxis.set_major_formatter(moneyFmt)
    ax.format_ydata = price
    ax.set_ylim(min(values + [-1.0]) * 1.1, max(values + [1.0]) * 1.1)

    ax.set_xticklabels([prettyname(account)])

    fig.autofmt_xdate()


    canvas=FigureCanvas(fig)
    response=HttpResponse(content_type='image/png')
    canvas.print_png(response)
    plt.close(fig)

    return response

def payee_boxplot(request, payee):

    data_parameters = ytd + ['-w', '-E', '-F', '%(amount)\n', '-M', 'reg', 'Expenses', 'and', 'payee', payee]

    output = runledger(data_parameters)
    values = [float(sanatize(line)) for line in output.split('\n') if line != ""]
    if(len(values) <= 0): values.append(0)

    fig = plt.figure()
    ax = fig.add_subplot(111)

    boxplot(values)

    title('Boxplot of ' + payee)
    ax.yaxis.set_major_formatter(moneyFmt)
    ax.format_ydata = price

    ax.set_xticklabels([prettyname(payee)])
    ax.set_ylim(min(values + [-1.0]) * 1.1, max(values + [1.0]) * 1.1)

    fig.autofmt_xdate()


    canvas=FigureCanvas(fig)
    response=HttpResponse(content_type='image/png')
    canvas.print_png(response)
    plt.close(fig)

    return response



def barchart(request, account):
    parameters = ytd + ['-c', '-E', '-w', '-j', '-M', 'reg'] + ["^" + account]

    output = runledger(parameters)

    times = []
    values = []
    for line in output.split('\n'):
        if(line == ""):
            continue
        times.append(datetime.datetime.strptime(line.split()[0], "%Y-%m-%d"))
        values.append(float(line.split()[1].replace(',', '.')))


    fig = plt.figure()
    ax = fig.add_subplot(111)

    ax.set_title("Monthly spending for " + account.split(':')[-1])

    ax.yaxis.set_major_formatter(moneyFmt)

    ax.format_ydata = price
    ax.grid(True)

    fig.autofmt_xdate()

    labels = [d.strftime("%B") for d in times]
    x = arange(len(labels))
    bar(x, values, align="center")

    xticks( x + 0.25,  labels )

    canvas=FigureCanvas(fig)
    response=HttpResponse(content_type='image/png')
    canvas.print_png(response)
    plt.close(fig)

    return response

def plot_subaccts(request, account):
    months   = mdates.MonthLocator()  # every month
    monthsFmt = mdates.DateFormatter('%m/%Y')

    fig = plt.figure()

    subaccounts = subacct_by_month(account)

    for sub, value in subaccounts.items():

        ax = fig.add_subplot(111)
        times = value['times']
        values = value['values']

        ax.plot_date(times, values, 'o-', xdate=True, label=prettyname(sub))

        ax.set_title("Subaccounts of " + prettyname(account))

        #Set properties of y(money) axis
        ax.xaxis.set_major_locator(months)
        ax.xaxis.set_major_formatter(monthsFmt)
        ax.yaxis.set_major_formatter(moneyFmt)

        #Set properties of x(time) axis
        daterange = (max(times) - min(times)).days
        daterange = datetime.timedelta(int(round(daterange * .05)))
        datemin = min(times) - daterange
        datemax = max(times) + daterange
        ax.set_xlim(datemin, datemax)

        # format the coords message box
        ax.format_xdata = mdates.DateFormatter('%Y-%m-%d')
        ax.format_ydata = price
        ax.grid(True)
        ax.legend()

    # rotates and right aligns the x labels, and moves the bottom of the
    # axes up to make room for them
    fig.autofmt_xdate()

    canvas=FigureCanvas(fig)
    response=HttpResponse(content_type='image/png')
    canvas.print_png(response)
    plt.close(fig)
    return response


def subacct_by_month(account):
    parameters = ['-c', '-p', 'this year', '-F', '%(account)\n', '--flat', 'bal', "^" + account]
    output = runledger(parameters)
    subaccounts = [line.strip() for line in output.split('\n') if line != ""]

    data = {}
    for sub in subaccounts:
        parameters = ytd + ['-c', '-E', '-w', '-j', '-M', 'reg'] + ["^" + sub]

        output = runledger(parameters)

        #Ignore subaccounts with no data
        if(output == ""):
            continue

        times = []
        values = []
        for line in output.split('\n'):
            if(line == ""):
                continue
            times.append(datetime.datetime.strptime(line.split()[0], "%Y-%m-%d").date())
            values.append(float(sanatize(line.split()[1])))

        data[sub] = {
             'times': times,
             'values': values
        }

    return data


def bar_subaccts(request, account):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    color_wheel = ['b', 'g', 'r', 'c', 'm', 'y', 'b']

    subaccounts = subacct_by_month(account)

    prev_sub = 0
    i = 0
    for sub, value in subaccounts.items():
        times = value['times']
        values = value['values']
        if(len(values) < 12):
            continue
        if prev_sub is 0:
            prev_sub = [0 for x in values]
        x = arange(len(times))
        bar(x, values, bottom = prev_sub, color = color_wheel[i%len(color_wheel)], label=prettyname(sub))
        xticks( x + 0.5,  times, label=prettyname(sub) )
        i = i+1
        prev_sub = [sum(pair) for pair in zip(prev_sub, values)]


    ax.set_title("Stacked bar chart of the subaccounts of " + prettyname(account))
    legend()

    #Set properties of y(money) axis
    ax.set_ylim(0,  max(prev_sub) * 1.1)
    ax.yaxis.set_major_formatter(moneyFmt)
    ax.format_ydata = price
    fig.autofmt_xdate()

    canvas=FigureCanvas(fig)
    response=HttpResponse(content_type='image/png')
    canvas.print_png(response)
    plt.close(fig)
    return response

def unbudgeted(request):

    parameters = ['-w', '-c', '-F', '%(account)\t%(amount)\n', '-p', 'this month', '--flat', '--no-total', '--unbudgeted', '-M', 'bal', '^exp']

    output = runledger(parameters)
    labels = []
    values = []
    for line in output.split('\n'):
        if(line == ""):
            continue
        label, value = line.split('\t')
        label = label.split(':')[-1]
        labels.append(label)
        value = float(sanatize(value))
        values.append(value)


    fig = plt.figure()
    ax = fig.add_subplot(111)

    ax.set_title('Unbudgeted spending this month')

    ax.yaxis.set_major_formatter(moneyFmt)
    ax.set_ylim(min(values + [0.0]) * 1.1, max(values + [0.0]) * 1.1)
    ax.format_ydata = price
    ax.grid(True)

    fig.autofmt_xdate()
    x = arange(len(labels))
    bar(x, values)
    xticks( x + 0.5,  labels )

    canvas=FigureCanvas(fig)
    response=HttpResponse(content_type='image/png')
    canvas.print_png(response)
    plt.close(fig)

    return response


