# Create your views here.
from django.http import HttpResponse
from django.template import Context, loader
from django.utils import simplejson

from ledger import *

import datetime
import vobject
import re

def index(request):
    t = loader.get_template('index.html')
    c = Context({
        "food_acct": "Expenses:Food",
        "discretionary": "Expenses:Discretionary",
        "days": 365,
        "retrospectives": ['Assets:Bank:FirstTech:Checking', 'Assets:Investment:AmericanFunds:Money Market' ],
    })
    return HttpResponse(t.render(c))

def extjs(request):
    t = loader.get_template('extjs.html')
    c = Context({
    })
    return HttpResponse(t.render(c))

def debugextjs(request):
    t = loader.get_template('debugextjs.html')
    c = Context({
    })
    return HttpResponse(t.render(c))



def accounts(request):
    parameters = ytd + ['accounts']

    output = runledger(parameters)

    accounts = [line for line in output.split('\n') if line != ""]

    t = loader.get_template('accounts.html')
    accounts.sort()
    c = Context({
        "accounts": accounts
    })

    return HttpResponse(t.render(c))

def accounts_json(request):
    parameters = ytd + ['accounts']
    query = request.GET
    limit = int(query.get('limit', 10000))
    start = int(query.get('start', 0))

    output = runledger(parameters)

    outputjson = {}
    outputjson['success'] = True
    outputjson['Accounts'] = []

    accounts = [line for line in output.split('\n') if line != ""]
    accounts.sort()
    if(limit):
        end = start + limit
    else:
        end = None

    for name in accounts[start:end]:
        outputjson['Accounts'].append({
                                        'Name': name,
                                     })
    return HttpResponse(
        simplejson.dumps(outputjson),
        content_type = 'application/javascript; charset=utf8'
    )

def payees(request):
    parameters = ytd + ['payees']
    output = runledger(parameters)
    payees = [line for line in output.split('\n') if line != ""]

    t = loader.get_template('payees.html')
    payees.sort()
    c = Context({
        "payees": payees
    })

    return HttpResponse(t.render(c))



def xacts_cal(request, account):
    parameters = ['-F', '%(date)\t%(payee)\t%(amount)\n', '-d', 'd>=[today]-14 & d < [tomorrow]',  '--sort', 'd', 'reg', '^' + account]
    output = runledger(parameters)

    cal = vobject.iCalendar()
    cal.add('method').value = 'PUBLISH'  # IE/Outlook needs this
    cal.add('x-wr-calname').value = prettyname(account) + " Events"
    cal.add('X-WR-TIMEZONE').value = "US/Pacific"
    cal.add('X-WR-CALDESC').value = "Daily events of account " + account

    for line in output.split('\n'):
        if(line == ""):
            continue
        date, payee, amount = line.split('\t')

        vevent = cal.add('vevent')
        vevent.add('summary').value = payee + "\t" + amount
        vevent.add('dtstart').value = datetime.datetime.strptime(date, "%Y/%m/%d").date()

    icalstream = cal.serialize()
    response = HttpResponse(icalstream, mimetype='text/calendar')
    response['Filename'] = 'ledger-events.ics'  # IE needs this
    response['Content-Disposition'] = 'attachment; filename=ledger-events.ics'

    return response


def total_cal(request, account):
    parameters = ['-E', '-p', 'daily', '-J', '-d', 'd>=[today]-14 & d < [tomorrow]',  '--sort', 'd', 'reg', '^' + account]
    output = runledger(parameters)

    cal = vobject.iCalendar()
    cal.add('method').value = 'PUBLISH'  # IE/Outlook needs this
    cal.add('x-wr-calname').value = prettyname(account) + " Balance"
    cal.add('X-WR-TIMEZONE').value = "US/Pacific"
    cal.add('X-WR-CALDESC').value = "Daily Balance of account " + account

    for line in output.split('\n'):
        if(line == ""):
            continue
        date, total = line.split(' ')

        vevent = cal.add('vevent')
        vevent.add('summary').value = moneyFmt(float(total), 0)
        vevent.add('dtstart').value = datetime.datetime.strptime(date, "%Y-%m-%d").date()

    icalstream = cal.serialize()
    response = HttpResponse(icalstream, mimetype='text/calendar')
    response['Filename'] = 'ledger-balance.ics'  # IE needs this
    response['Content-Disposition'] = 'attachment; filename=ledger-balance.ics'

    return response

def budget(request):
    parameters = ['-c', '--flat', '-p', 'this month', 'budget', 'expenses', 'investment' ]

    output = runledger(parameters)

    data = []
    for line in output.split('\n'):
        if(line == ""):
            continue
        if(line[0] == '-' and line[1] == '-'):
            continue
        line = re.sub("  +", "\t", line.strip())
        data.append(line.split('\t'))

    #End of month estimate calculation
    balance = float(sanatize(runledger(['-F', '%(amount)', '-c', 'bal', 'FirstTech:Checking'])))
    budget_list = sanatize(runledger(['-c', '-p', 'this month', '-F', '%(total)', '--flat', '--budget', '-M', 'bal', '^exp', 'liab', 'invest']))
    budget = float(budget_list.split()[-1])
    eom = balance + budget



    t = loader.get_template('budget.html')
    c = Context({
        "data": data,
        "eom": price(eom, 0),
        "balance": balance,
        "budget": budget
    })

    return HttpResponse(t.render(c))


def forecast(request):
    account = "Assets:Bank:FirstTech:Checking"
    days = 365
    parameters = ['-w', '-F', '%(date)\t%(payee)\t%(amount)\t%(total)\t%(note)\n', '--forecast', 'd<=[today]+'+str(days), '-d', 'd>[today] & d<[today]+'+str(days), '--sort', 'd', 'reg', account]


    output = runledger(parameters)

    data = [line.split('\t') for line in output.split('\n') if line != ""]

    t = loader.get_template('forecast.html')
    c = Context({
        "forecasts": ['Assets:Bank:FirstTech:Checking', 'Assets:Investment:AmericanFunds:Money Market'],
        "account": prettyname(account),
        "days": days,
        "data": data
    })

    return HttpResponse(t.render(c))

def checking(request):
    account = "Assets:Bank:FirstTech:Checking"
    x = 10
    parameters = ['-w', '-F', '%(date)\t%(payee)\t%(amount)\t%(total)\t%(note)\n', '-c', '--tail', str(x), '--sort', 'd', 'reg', account]

    output = runledger(parameters)

    data = [line.split('\t') for line in output.split('\n') if line != ""]

    t = loader.get_template('checking.html')
    c = Context({
        "x": x,
        "account": prettyname(account),
        "data": data,
        "daily_account": account
    })

    return HttpResponse(t.render(c))

def checking_json(request):
    account = "Assets:Bank:FirstTech:Checking"
    parameters = ['-w', '-F', '%(date)\t%(payee)\t%(amount)\t%(total)\t%(note)\n', '-c', '--sort', 'd', 'reg', account]
    query = request.GET
    limit = query.get('limit', None)

    if(limit):
        parameters += ['--tail', str(limit)]

    output = runledger(parameters)

    outputjson = {}
    outputjson['success'] = True
    outputjson['CheckingItems'] = []
    for line in output.split('\n'):
        if(line == ""):
            continue
        date, payee, amount, balance, notes = line.split('\t')
        amount = sanatize(amount)
        balance = sanatize(balance)
        outputjson['CheckingItems'].append({
                                        'Date': date,
                                        'Payee': payee,
                                        'Amount': amount,
                                        'Balance': balance,
                                        'Notes': notes,
                                     })
    return HttpResponse(
        simplejson.dumps(outputjson),
        content_type = 'application/javascript; charset=utf8'
    )



