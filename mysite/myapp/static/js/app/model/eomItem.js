Ext.define('pygl.model.eomItem', {
    extend: 'Ext.data.Model',
    idProperty: 'eom',
    fields: [
        { name: 'eom', type: 'float' }
    ]
});


