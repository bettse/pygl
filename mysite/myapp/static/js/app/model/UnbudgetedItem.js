Ext.define('pygl.model.UnbudgetedItem', {
    extend: 'Ext.data.Model',
    idProperty: 'Account',
    fields: [
        { name: 'Amount', type: 'float' },
        { name: 'Account', type: 'string' }
    ]
});


