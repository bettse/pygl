Ext.define('pygl.model.RegisterItem', {
    extend: 'pygl.model.BalanceItem',
    fields: [
        { name: 'Date', type: 'date'},
        { name: 'Amount', type: 'float' }
    ]
});

