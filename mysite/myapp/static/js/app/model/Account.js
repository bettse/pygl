Ext.define('pygl.model.Account', {
    extend: 'Ext.data.Model',
    idProperty: 'Name',
    fields: [
        { name: 'Name', type: 'string'},
    ],
    hasMany: {model: 'BalanceItem', name: 'balance'},
    hasMany: {model: 'RegisterItem', name: 'register'},
    hasMany: {model: 'ActivityItem', name: 'activity'},
    proxy: {
        type: 'rest',
        url : 'json/accounts',
        reader: {
            type: 'json',
            root: 'Accounts',
            successProperty: 'success',
            totalProperty: 'total'
        }
    }


});
