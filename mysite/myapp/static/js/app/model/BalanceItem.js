Ext.define('pygl.model.BalanceItem', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'Date', type: 'date', dateFormat: 'Y-m-d'},
        { name: 'Amount', type: 'float' }
    ],
    proxy: {
        type: 'ajax',
        url : 'json/balance/',
        limitParam: undefined,
        reader: {
                type: 'json',
                root: 'BalanceItems',
                successProperty: 'success'
            }
    }

});

