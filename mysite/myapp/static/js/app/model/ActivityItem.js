Ext.define('pygl.model.ActivityItem', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'Date', type: 'date', dateFormat: 'Y/m/d' },
        { name: 'Payee', type: 'string' },
        { name: 'Amount', type: 'float' },
        { name: 'Balance', type: 'float' },
        { name: 'Notes', type: 'string' }
    ]
});


