Ext.define('pygl.model.BudgetItem', {
    extend: 'Ext.data.Model',
    idProperty: 'Account',
    fields: [
        { name: 'Budgeted', type: 'float' },
        { name: 'Percentage', type: 'float' },
        { name: 'Difference', type: 'float' },
        { name: 'Spent', type: 'float' },
        { name: 'Account', type: 'string' }
    ]
});


