Ext.require('Ext.chart.*');

Ext.define('pygl.view.forecast.CheckingYear' ,{
    extend: 'Ext.chart.Chart',
    alias: 'widget.checkingforecast',
    title: 'Forecast of Checking',
    width: 800,
    height: 300,
    store: 'ForecastChecking',
    axes: [{
        type: 'Numeric',
        position: 'left',
        fields: ['Amount'],
        label: {
            renderer: Ext.util.Format.usMoney
        },
        grid: true,
        minimum: 0
    },{
        type: 'Time',
        position: 'bottom',
        fields: ['Date'],
        dateFormat: 'M d',
        label: { rotate: { degrees: 315 }}
    }],
    series: [{
        type: 'line',
        highlight: {
            size: 7,
            radius: 7
        },
        tips: {
          trackMouse: true,
          width: 70,
          height: 20,
          renderer: function(storeItem, item) {
            this.setTitle("$" + storeItem.get('Amount'));
          }
        },
        axis: ['left', 'bottom'],
        xField: 'Date',
        yField: 'Amount'
    }]

});
