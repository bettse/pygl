Ext.require('Ext.chart.*');

Ext.define('pygl.view.forecast.MoneyMarketYear' ,{
    extend: 'pygl.view.forecast.CheckingYear',
    alias: 'widget.mmforecast',
    title: 'Forecast of Money Market',
    width: 800,
    height: 300,
    store: 'ForecastMoneyMarket'
});
