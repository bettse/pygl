Ext.require('Ext.chart.*');

Ext.define('pygl.view.Activity' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.activity',
    title: 'Recent Activity',
    store: 'RecentActivity',

    columns: [{
                header:'Date', 
                dataIndex: 'Date', 
                renderer: Ext.util.Format.dateRenderer('M d, Y') 
            },{
                header:'Payee', 
                dataIndex: 'Payee'
            },{
                header:'Amount', 
                dataIndex: 'Amount', 
                renderer: Ext.util.Format.usMoney
            },{
                header:'Balance', 
                dataIndex: 'Balance', 
                renderer: Ext.util.Format.usMoney
            },{
                header:'Notes', 
                dataIndex: 'Notes', 
                flex: 1
    }],
    autoHeight: true,
    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: 'RecentActivity',   // same store GridPanel is using
        dock: 'bottom',
        displayInfo: true
    }]


});
