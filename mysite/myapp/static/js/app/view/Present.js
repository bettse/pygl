Ext.define('pygl.view.Present' ,{
    extend: 'Ext.Panel',
    alias: 'widget.present',
    title: 'This Month',
    height: 750,
    width: 1000,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    items: [{
        xtype: 'panel',
        height: 300,
        width: 1000,
        layout: {
            type: 'hbox',
            align: 'stretch'
        },
        items: [{
            xtype: 'activity',
            store: 'RecentActivity',
            flex: 2
        },{

            xtype: 'panel',
            title: 'First Tech This Month',
            layout: 'fit',
            items: {
                xtype: 'monthbalance',
                account: 'Assets:Bank:FirstTech:Checking',
                duration: 'month',
                animate: false
            },
            flex: 2
        },{
            xtype: 'unbudgeted',
            animate: true,
            flex: 1
        }]

    },{
        xtype: 'panel',
        height: 350,
        width: 1000,
        layout: {
            type: 'hbox',
            align: 'stretch'
        },
        items: [{
        },{
            xtype: 'budgetprogress',
            flex: 1
        },{
            xtype: 'budgetoverunder',
            flex: 1,
            autoHeight: true,
            animate: true
        }]


    }]


});
