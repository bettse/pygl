Ext.define('pygl.view.account.List' ,{
    alias: 'widget.accountlist',
    store: 'Accounts',
    extend: 'Ext.grid.Panel',
    title: "List of all accounts",
    columns: [{
                header:'Name', 
                dataIndex: 'Name', 
                flex: 1
    }],
    autoHeight: true,
    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: 'Accounts',   // same store GridPanel is using
        dock: 'bottom',
        displayInfo: true
    }]


});





