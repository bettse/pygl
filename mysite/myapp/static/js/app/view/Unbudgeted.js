Ext.require('Ext.chart.*');

Ext.define('pygl.view.Unbudgeted' ,{
    extend: 'Ext.chart.Chart',
    alias: 'widget.unbudgeted',
    title: 'Unbudgeted spending this month',
    width: 800,
    height: 300,
    store: 'UnbudgetedItems',

    axes: [{
        type: 'Numeric',
        position: 'left',
        fields: ['Amount'],
        label: {
            renderer: Ext.util.Format.usMoney
        },
        grid: true,
        minimum: 0
    }, {
        type: 'Category',
        position: 'bottom',
        fields: ['Account'],
        label: { rotate: { degrees: 315 }}
    }],
    series: [{
        type: 'column',
        axis: 'left',
        highlight: true,
        tips: {
          trackMouse: true,
          width: 140,
          height: 28,
          renderer: function(storeItem, item) {
            this.setTitle(storeItem.get('Account') + ': $' + storeItem.get('Amount'));
          }
        },
        label: {
          display: 'outsideEnd',
          'text-anchor': 'middle',
            field: 'Amount',
            renderer: Ext.util.Format.usMoney,
            orientation: 'horizontal',
            color: '#333'
        },
        xField: 'Account',
        yField: 'Amount'
    }]

});
