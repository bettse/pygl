Ext.require('Ext.chart.*');

Ext.define('pygl.view.AssetPie' ,{
    extend: 'Ext.chart.Chart',
    alias: 'widget.assetpie',
    store: 'Assets',
    title: 'Asset Breakdown',
    shadow: false,
    series: [{
        type: 'pie',
        field: 'Amount',
        tips: {
          trackMouse: true,
          width: 160,
          height: 28,
          renderer: function(storeItem, item) {
            this.setTitle(storeItem.get('Account') + ': $' + storeItem.get('Amount') );
          }
        },
        label: {
            field: 'Account',
            display: 'rotate',
            contrast: true,
            renderer: this.shortlabel
        }
    }],
    shortlabel: function (val){
        var length = 10;
        val = Ext.util.Format.lowercase(val);
        val = Ext.util.Format.capitalize(val);
        if(val.length > length){
            return val.substring(0,length) + '...';
        }else{
            return val;
        }
    }

});
