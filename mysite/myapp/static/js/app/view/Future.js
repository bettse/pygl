Ext.define('pygl.view.Future' ,{
    extend: 'Ext.Panel',
    alias: 'widget.future',
    title: 'Future',
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    items: [{
        xtype: 'panel',
        height: 900,
        width: 600,
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        items: [{
            xtype: 'panel',
            title: 'Forecast of Checking',
            layout: 'fit',
            items: {
                xtype: 'checkingforecast',
                animate: true
            },
            flex: 1
        },{
            xtype: 'panel',
            title: 'Forecast of Money Market',
            layout: 'fit',
            items: {
                xtype: 'mmforecast',
                animate: true
            },
            flex: 1
        }]
    },{
        xtype: 'panel',
        height: 900,
        width: 600,
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        items: [{

            xtype: 'activity',
            store: 'ForecastActivity',
            title: 'Forecast Activity',
            animate: true,
            dockedItems: [{
                xtype: 'pagingtoolbar',
                store: 'ForecastActivity',   // same store GridPanel is using
                dock: 'bottom',
                displayInfo: true
            }],
            flex: 1

        },{
            xtype: 'assetpie',
            animate: true,
            flex: 1
        }]
    }]
});
