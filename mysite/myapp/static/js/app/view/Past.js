Ext.define('pygl.view.Past' ,{
    extend: 'Ext.Panel',
    alias: 'widget.past',
    title: 'Past',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    items: [{
        xtype: 'panel',
        title: 'Checking Balance YTD',
        layout: 'fit',
        items: {
            xtype: 'monthbalance',
            account: 'Assets:Bank:FirstTech:Checking',
            duration: 'year',
            title: 'Balance for last 365 days',
            animate: false
        },
        flex: 1
    },{
        xtype: 'panel',
        title: 'Money Market Balance YTD',
        layout: 'fit',
        items: {
            xtype: 'monthbalance',
            account: 'Assets:Investment:AmericanFunds:Money Market',
            duration: 'year',
            title: 'Money Market Balance',
            animate: false
        },
        flex: 1
    },{
        xtype: 'budgetpie',
        flex: 1
    }]
});
