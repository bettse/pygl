Ext.require('Ext.chart.*');

Ext.define('pygl.view.balance.Month' ,{
    extend: 'Ext.chart.Chart',
    alias: 'widget.monthbalance',
    width: 800,
    height: 300,
    animate: false,
    axes: [{
        type: 'Numeric',
        position: 'left',
        fields: ['Amount'],
        label: {
            renderer: Ext.util.Format.usMoney
        },
        title: 'Balance',
        grid: true,
        minimum: 0
    },{
        type: 'Time',
        position: 'bottom',
        fields: ['Date'],
        dateFormat: 'M d',
        label: { rotate: { degrees: 315 }}
    }],
    series: [{
        type: 'line',
        highlight: {
            size: 7,
            radius: 7
        },
        tips: {
          trackMouse: true,
          width: 70,
          height: 20,
          renderer: function(storeItem, item) {
            this.setTitle(Ext.util.Format.usMoney(storeItem.get('Amount')));
          }
        },
        axis: ['left', 'bottom'],
        xField: 'Date',
        yField: 'Amount'
    }],

    initComponent: function(){
        //console.log("Loading " + this.account + "[" + this.duration + "]");
        this.store = Ext.create('Ext.data.Store', {
            model: 'pygl.model.BalanceItem'
        });

        this.store.load({
            params: {
                account: this.account,
                duration: this.duration
            }
        });
        this.callParent(arguments);
    }
});
