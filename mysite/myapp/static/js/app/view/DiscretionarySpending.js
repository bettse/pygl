Ext.require('Ext.chart.*');

Ext.define('pygl.view.DiscretionarySpending' ,{
    extend: 'Ext.chart.Chart',
    alias: 'widget.discretionaryspending',
    store: 'DiscretionarySpending',
    title: "Discretionary spending by month",
    width: 800,
    height: 300,

    axes: [{
        type: 'Numeric',
        position: 'left',
        fields: ['Amount'],
        label: {
            renderer: Ext.util.Format.usMoney
        },
        title: 'Amount Spent',
        grid: true
    }, {
        type: 'Time',
        position: 'bottom',
        fields: ['Date'],
        title: 'Month',
        dateFormat: 'M',
        step: [Ext.Date.MONTH, 1],
        label: { rotate: { degrees: 315 }}
    }],
    series: [{
        type: 'column',
        axis: ['left', 'bottom'],
        highlight: true,
        xField: 'Date',
        yField: 'Amount'
    }]




});
