function moneycolor(val){
    val = val.toFixed(2);
    if(val < 0){
        return '<span style="color:green;">' + Ext.util.Format.usMoney(val*-1) + '</span>';
    }else if(val > 0){
        return '<span style="color:red;">' + Ext.util.Format.usMoney(val*-1) + '</span>';
    }
    return Ext.util.Format.usMoney(val);
}

// example of custom renderer function
function percentcolor(val){
    if(val < 100){
        return '<span style="color:green;">' + val + '%</span>';
    }else if(val > 100){
        return '<span style="color:red;">' + val + '%</span>';
    }
    return val + "%";
}

Ext.define('pygl.view.budget.ProgressGrid' ,{
    alias: 'widget.budgetprogress',
    store: 'BudgetSansChecking',
    extend: 'Ext.grid.Panel',
    title: "This month's budget progress",
    columns: [{
                header:'Spent',
                dataIndex: 'Spent',
                renderer: Ext.util.Format.usMoney
            },{
                header:'Budgeted',
                dataIndex: 'Budgeted',
                renderer: Ext.util.Format.usMoney
            },{
                header:'Percent spent',
                dataIndex: 'Percentage',
                renderer: percentcolor
            },{
                header:'Difference',
                dataIndex: 'Difference',
                renderer: moneycolor
            },{
                header:'Account',
                dataIndex: 'Account',
                renderer: function(val){
                    val = Ext.util.Format.lowercase(val);
                    val = Ext.util.Format.capitalize(val);
                    return val;
                },
                flex: 1
    }],
    autoHeight: true,
    bbar: [{
        text: "",
        id: 'eombutton'
    }]

});


