Ext.require('Ext.chart.*');



Ext.define('pygl.view.budget.Pie' ,{
    extend: 'Ext.chart.Chart',
    alias: 'widget.budgetpie',
    store: 'BudgetSansChecking',

    title: 'Montly budget breakdown',
    shadow: false,
    //theme: 'Demo',
    series: [{
        type: 'pie',
        field: 'Budgeted',
        showInLegend: true,
        donut: 25,
        tips: {
          trackMouse: true,
          width: 160,
          height: 28,
          renderer: function(storeItem, item) {
            this.setTitle(storeItem.get('Account') + ': $' + storeItem.get('Budgeted') );
          }
        },
        highlight: {
          segment: {
            margin: 20
          }
        },
        label: {
            field: 'Account',
            display: 'rotate',
            contrast: true,
            renderer: this.shortlabel
        }
    }],
    shortlabel: function (val){
        var length = 10;
        val = Ext.util.Format.lowercase(val);
        val = Ext.util.Format.capitalize(val);
        if(val.length > length){
            return val.substring(0,length) + '...';
        }else{
            return val;
        }
    }



});
