Ext.require('Ext.chart.*');

Ext.define('pygl.view.budget.OverUnder' ,{
    extend: 'Ext.chart.Chart',
    alias: 'widget.budgetoverunder',
    store: 'InvertedBudgetSansChecking',
    title: "Buget Over/Under",
    width: 800,
    height: 600,

    axes: [{
        type: 'Numeric',
        position: 'left',
        fields: ['Difference'],
        label: {
            renderer: Ext.util.Format.usMoney
        },
        grid: true
    }, {
        type: 'Category',
        position: 'bottom',
        fields: ['Account'],
        label: {
            rotate: { degrees: 315 },
            renderer: function(val){
                var length = 10;
                val = Ext.util.Format.lowercase(val);
                val = Ext.util.Format.capitalize(val);
                if(val.length > length){
                    return val.substring(0,length) + '...';
                }else{
                    return val;
                }
            }
        }
    }],
    series: [{
        type: 'column',
        axis: 'left',
        renderer: function(sprite, record, attributes, index, store) {
            if(record.data.Difference < 0){
                attributes.fill = '#f41';
            }
            return attributes;
        },
        tips: {
          trackMouse: true,
          width: 140,
          height: 28,
          renderer: function(storeItem, item) {
            this.setTitle(storeItem.get('Account') + ': $' + storeItem.get('Difference'));
          }
        },

        highlight: true,
        xField: 'Account',
        yField: 'Difference'
    }]


});
