Ext.require('pygl.model.UnbudgetedItem');

Ext.define('pygl.store.UnbudgetedItems', {
    extend: 'Ext.data.Store',
    model: 'pygl.model.UnbudgetedItem',
    proxy: {
        type: 'ajax',
        url : 'json/unbudgeted',
        reader: {
                type: 'json',
                root: 'unbudgeted',
                successProperty: 'success'
            }
    },
    autoLoad: true
});


