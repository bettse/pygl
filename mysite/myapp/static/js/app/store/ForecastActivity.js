Ext.require('pygl.model.ActivityItem');

Ext.define('pygl.store.ForecastActivity', {
    extend: 'Ext.data.Store',
    model: 'pygl.model.ActivityItem',
    pageSize: 20,
    sorters: [
        {
         property : 'Date',
         direction: 'ASC'
    }],
    proxy: {
        type: 'ajax',
        url : 'json/activity/forecast/Assets:Bank:FirstTech:Checking',
        reader: {
                type: 'json',
                root: 'ActivityItems',
                successProperty: 'success'
            }
    },
    autoLoad: true
});


