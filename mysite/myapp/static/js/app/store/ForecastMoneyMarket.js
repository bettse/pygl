Ext.require('pygl.model.BalanceItem');

Ext.define('pygl.store.ForecastMoneyMarket', {
    extend: 'Ext.data.Store',
    model: 'pygl.model.BalanceItem',
    proxy: {
        type: 'ajax',
        url : 'json/forecast/Assets:Investment:AmericanFunds:Money%20Market',
        limitParam: undefined,
        reader: {
                type: 'json',
                root: 'BalanceItems',
                successProperty: 'success'
            }
    },
    autoLoad: true
});


