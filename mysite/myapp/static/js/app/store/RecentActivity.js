Ext.require('pygl.model.ActivityItem');

Ext.define('pygl.store.RecentActivity', {
    extend: 'Ext.data.Store',
    model: 'pygl.model.ActivityItem',
    pageSize: 11,
    autoLoad: true,
    proxy: {
        type: 'ajax',
        url : 'json/activity/recent/Assets:Bank:FirstTech:Checking',
        reader: {
                type: 'json',
                root: 'ActivityItems',
                successProperty: 'success',
                totalProperty: 'total'
            }
    }
});


