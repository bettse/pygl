Ext.require('pygl.model.eomItem');

Ext.define('pygl.store.eomItems', {
    extend: 'Ext.data.Store',
    model: 'pygl.model.eomItem',
    proxy: {
        type: 'ajax',
        url : 'json/eomestimate',
        reader: {
                type: 'json',
                successProperty: 'success'
            }
    },
    autoLoad: true
});


