Ext.require('pygl.model.Asset');

Ext.define('pygl.store.Assets', {
    extend: 'Ext.data.Store',
    model: 'pygl.model.Asset',
    proxy: {
        type: 'ajax',
        url : 'json/assets',
        reader: {
                type: 'json',
                root: 'assets',
                successProperty: 'success'
            }
    },
    autoLoad: true
});


