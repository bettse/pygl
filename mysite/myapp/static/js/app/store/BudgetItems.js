Ext.require('pygl.model.BudgetItem');

Ext.define('pygl.store.BudgetItems', {
    extend: 'Ext.data.Store',
    model: 'pygl.model.BudgetItem',
    proxy: {
        type: 'ajax',
        url : 'json/budget',
        reader: {
                type: 'json',
                root: 'budget',
                successProperty: 'success'
            }
    },
    autoLoad: true
});


