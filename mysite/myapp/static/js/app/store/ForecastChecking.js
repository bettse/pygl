Ext.require('pygl.model.BalanceItem');

Ext.define('pygl.store.ForecastChecking', {
    extend: 'Ext.data.Store',
    model: 'pygl.model.BalanceItem',
    proxy: {
        type: 'ajax',
        url : 'json/forecast/Assets:Bank:FirstTech:Checking',
        limitParam: undefined,
        reader: {
                type: 'json',
                root: 'BalanceItems',
                successProperty: 'success'
            }
    },
    autoLoad: true
});


