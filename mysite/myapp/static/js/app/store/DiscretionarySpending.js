Ext.require('pygl.model.RegisterItem');

Ext.define('pygl.store.DiscretionarySpending', {
    extend: 'Ext.data.Store',
    model: 'pygl.model.RegisterItem',
    proxy: {
        type: 'ajax',
        url : 'json/spending/year/Expenses:Discretionary',
        reader: {
                type: 'json',
                root: 'SpendingItems',
                successProperty: 'success'
            }
    },
    autoLoad: true
});


