Ext.require('pygl.model.Account');

Ext.define('pygl.store.Accounts', {
    extend: 'Ext.data.Store',
    model: 'pygl.model.Account',
    proxy: {
        type: 'ajax',
        url : 'json/accounts',
        reader: {
                type: 'json',
                root: 'accounts',
                successProperty: 'success'
            }
    },
    autoLoad: true
});


