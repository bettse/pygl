Ext.define('pygl.controller.Activity', {
    extend: 'Ext.app.Controller',

    views: ['Activity'],
    models: ['ActivityItem'],
    stores: ['RecentActivity', 'ForecastActivity']

});
