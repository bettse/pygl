Ext.define('pygl.controller.Forecast', {
    extend: 'Ext.app.Controller',

    views: ['forecast.CheckingYear', 'forecast.MoneyMarketYear'],
    models: ['BalanceItem'],
    stores: ['ForecastChecking', 'ForecastMoneyMarket']

});
