Ext.define('pygl.controller.Unbudgeted', {
    extend: 'Ext.app.Controller',

    views: ['Unbudgeted'],
    models: ['UnbudgetedItem'],
    stores: ['UnbudgetedItems']

});
