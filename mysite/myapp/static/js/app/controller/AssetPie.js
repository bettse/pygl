Ext.define('pygl.controller.AssetPie', {
    extend: 'Ext.app.Controller',

    views: ['AssetPie'],
    models: ['Asset'],
    stores: ['Assets']

});
