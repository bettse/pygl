Ext.define('pygl.controller.Past', {
    extend: 'Ext.app.Controller',

    views: ['Past', 'balance.Month', 'budget.Pie'],

    refs: [
        {ref: 'yearchart', selector: 'monthbalance'}
    ],

    onLaunch: function(){
        this.getYearchart().series.smooth = true;
    }


});

