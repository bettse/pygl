Ext.define('pygl.controller.Discretionary', {
    extend: 'Ext.app.Controller',

    views: ['DiscretionarySpending'],
    models: ['RegisterItem'],
    stores: ['DiscretionarySpending']

});
