Ext.define('pygl.controller.Accounts', {
    extend: 'Ext.app.Controller',

    views: ['account.List'],
    models: ['Account'],
    stores: ['Accounts'],

    init: function() {
        this.control({
            'accountlist': {
                itemdblclick: this.loadAccount
            }
        });
    },


    loadAccount: function(grid, record) {
        console.log('Double clicked on ' + record.get('Name'));
        var store = Ext.create('Ext.data.Store', {
            model: 'pygl.model.BalanceItem'
        });

        store.load({
            params: {
                account: record.get('Name'),
                duration: 'year'
            }
        });

    }

});
