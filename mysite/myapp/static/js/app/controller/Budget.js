Ext.define('pygl.controller.Budget', {
    extend: 'Ext.app.Controller',

    views: ['budget.Pie', 'budget.ProgressGrid', 'budget.OverUnder'],
    models: ['BudgetItem', 'eomItem'],
    stores: ['BudgetItems', 'eomItems'],

    refs: [
        { ref: 'eombutton', selector: '#eombutton'}
    ],

    removeChecking: function(item){
        return item.get("Account") != "Checking"  && item.get("Account") != "Money Market" && item.get("Difference") != "0";
    },

    init: function(){
        var store;

        Ext.create('pygl.store.BudgetItems', {
            model: 'BudgetItem',
            storeId: 'BudgetSansChecking'
        });

        Ext.create('pygl.store.BudgetItems', {
            model: 'BudgetItem',
            storeId: 'InvertedBudgetSansChecking'
        });

        store = Ext.getStore('BudgetSansChecking');

        store.filter({filterFn: this.removeChecking});

        store = Ext.getStore('InvertedBudgetSansChecking');
        store.filter({filterFn: this.removeChecking});

        store.on('load', function(store, records, options){
            store.each(function(record){
                record.set('Difference', record.get('Difference') * -1);
                return true;
            }, this);
        });


    },

    //Load the Eom to the button during load
    onLaunch: function(){
        //Save reference to the button since 'this' is about to change
        eombutton = this.getEombutton();
        //Set callback for when the store is loaded
        this.getEomItemsStore().on('load', function() {
            //Pretty simple to set the text using the previously saved button reference
            eombutton.setText("End of month estimate: " + Ext.util.Format.usMoney(this.first().get('eom')));
        });
    }

});
