Ext.define('pygl.controller.Balance', {
    extend: 'Ext.app.Controller',

    views: ['balance.Month'],
    models: ['BalanceItem', 'eomItem'],
    stores: ['eomItems', 'BalanceItems'],

    init: function(){
    },

    onLaunch: function(){
        bController = this;
        this.getEomItemsStore().on('load', function() {
            //Load EOM value into month balance store
            eom = this.first().get('eom');
            var lastday = Ext.Date.format(Ext.Date.getLastDateOfMonth(new Date()), "Y-n-d");
            var myRecord = {
                Date: lastday,
                Amount: eom
            };
            //bController.getMonthBalanceStore().add(myRecord);
        });
    }
});
