Ext.Loader.setConfig({
    enabled : true,
    paths   : {
        pygl : '/site_media/js/app'
    }
//    disableCaching: false
});

//Ext.Ajax.disableCaching = false;

Ext.require('Ext.chart.*');

Ext.application({
    name: 'pygl',
    appFolder: '/site_media/js/app',
    models: ['Asset', 'BudgetItem', 'UnbudgetedItem', 'BalanceItem', 'RegisterItem', 'ActivityItem'],
    stores: ['BudgetItems', 'Assets', 'UnbudgetedItems', 'DiscretionarySpending'],
    controllers: ['Budget', 'Discretionary', 'Balance', 'Unbudgeted', 'Activity', 'Forecast', 'AssetPie', 'Present', 'Past', 'Future', 'Accounts'],
    views: ['Present', 'Past', 'Future'],
    launch: function() {

        var loadingMask = Ext.get('loading-mask');
        var loading = Ext.get('loading');
        //  Hide loading message
        loading.fadeOut({ duration: 0.2, remove: true });
        //  Hide loading mask
        loadingMask.setOpacity(0.9);
        loadingMask.shift({
            xy: loading.getXY(),
            width: loading.getWidth(),
            height: loading.getHeight(),
            remove: true,
            duration: 1,
            opacity: 0.1,
            easing: 'bounceOut'
        });


        var tabs = Ext.create('Ext.tab.Panel', {
            layout: 'fit',
            activeTab: 1,
            height: '100%',
            width: '100%',
            defaults: {
                bodyPadding: 10
            },
            items: [
                { xtype: 'past' },
                { xtype: 'present' },
                { xtype: 'future' },
                { xtype: 'accountlist' }
            ]
        });
        Ext.create('Ext.container.Viewport', {
            layout: 'fit',
            height: 768,
            width: 1024,
            items: [tabs]
        });
    }
});



