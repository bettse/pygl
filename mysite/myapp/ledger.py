from subprocess import Popen,PIPE
import sys, os

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.dates import DateFormatter
import matplotlib.dates as mdates
import matplotlib.mlab as mlab
import matplotlib.cbook as cbook
import matplotlib.ticker as ticker

import locale
locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')

ytd = ['-d', 'd > [last month]-365days & d<[this month]']

def price(x, pos):
    return unicode(locale.currency(x, grouping=True), 'utf-8')

moneyFmt = ticker.FuncFormatter(price)

def sanatize(money):
    return money.replace('$','').replace(',','').strip()

def runledger(parameters):
    LEDGER_FILE='/Users/bettse/Dropbox/Finances/current.lgr'
    ledger = 'ledger'
    for f in ['/usr/bin/ledger', '/usr/local/bin/ledger']:
        if os.path.exists(f):
            ledger = f
            break

    return Popen([ledger, '-f', LEDGER_FILE, '--no-color'] + parameters, stdout=PIPE).communicate()[0]

def prettyname(account):
        return account.split(':')[-1]


