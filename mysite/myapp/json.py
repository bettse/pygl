# Create your views here.
from django.http import HttpResponse
from django.template import Context, loader
from django.utils import simplejson

from ledger import *

import datetime
import re


def budget(request):
    parameters = ['-c', '--no-total', '--flat', '-p', 'this month', 'budget', 'expenses', 'investment', 'checking' ]

    query = request.GET
    limit = int(query.get('limit', 10000))
    start = int(query.get('start', 0))

    output = runledger(parameters)

    if(limit):
        end = start + limit
    else:
        end = None

    outputjson = {}
    outputjson['success'] = True
    outputjson['budget'] = []

    for line in output.split('\n')[start:end]:
        if(line == ""): continue
        line = line.replace('$', '').replace(',','').replace('%','')
        spent, budgeted, difference, percentage, account = line.split(None, 4)
        outputjson['budget'].append({
                                        'Spent': spent,
                                        'Budgeted': budgeted,
                                        'Difference': difference,
                                        'Percentage': percentage,
                                        'Account': prettyname(account)
                                     })

    return HttpResponse(
        simplejson.dumps(outputjson),
        content_type = 'application/javascript; charset=utf8'
    )

def unbudgeted(request):
    query = request.GET
    limit = int(query.get('limit', 10000))
    start = int(query.get('start', 0))

    if(limit):
        end = start + limit
    else:
        end = None

    outputjson = {}
    outputjson['success'] = True
    outputjson['unbudgeted'] = []

    parameters = ['-w', '-c', '-F', '%(account)\t%(amount)\n', '-p', 'this month', '--flat', '--no-total', '--unbudgeted', '-M', 'bal', '^exp']

    output = runledger(parameters)
    for line in output.split('\n')[start:end]:
        if(line == ""):
            continue
        account, value = line.split('\t')

        outputjson['unbudgeted'].append({
                                        'Account': prettyname(account),
                                        'Amount': sanatize(value)
                                     })

    return HttpResponse(
        simplejson.dumps(outputjson),
        content_type = 'application/javascript; charset=utf8'
    )


def yearbalance(request, account):
    base_parameters = ['-E', '--sort', 'd', '--weekly', '-J']
    parameters = base_parameters + ['-d', 'd<[today] & d>[today]-365', 'register', "^" + account]
    return balance(request, parameters)

def monthbalance(request, account=None):
    query = request.GET

    if(not account):
        account = query.get('account', '')

    parameters = ['--sort', 'd', '-E', '--daily', '-J', '-p', 'daily', '-d', 'd>=[this month] & d <= [today]', 'register'] + ["^" + account]
    return balance(request, parameters)

def newbalance(request):
    query = request.GET
    account = query.get('account', '')
    duration = query.get('duration', '')
    if(duration == 'month'):
        parameters = ['--sort', 'd', '-E', '--daily', '-J', '-p', 'daily', '-d', 'd>=[this month] & d <= [today]', 'register'] + ["^" + account]
    elif (duration == 'year'):
        parameters = ['-E', '--sort', 'd', '--weekly', '-J', '-d', 'd<[today] & d>[today]-365', 'register', "^" + account]

    return balance(request, parameters)



def balance(request, parameters):
    query = request.GET
    limit = int(query.get('limit', 10000))
    start = int(query.get('start', 0))

    if(limit):
        end = start + limit
    else:
        end = None

    outputjson = {}
    outputjson['success'] = True
    outputjson['BalanceItems'] = []

    output = runledger(parameters)

    for line in output.split('\n')[start:end]:
        if(line == ""):
            continue
        outputjson['BalanceItems'].append({
                                        'Date': line.split()[0],
                                        'Amount': sanatize(line.split()[1])
                                     })

    return HttpResponse(
        simplejson.dumps(outputjson),
        content_type = 'application/javascript; charset=utf8'
    )




def yearspending(request, account):
    query = request.GET
    limit = int(query.get('limit', 10000))
    start = int(query.get('start', 0))

    if(limit):
        end = start + limit
    else:
        end = None

    outputjson = {}
    outputjson['success'] = True
    outputjson['SpendingItems'] = []

    parameters = ytd + ['-c', '-E', '-w', '-j', '-M', 'reg'] + ["^" + account]

    output = runledger(parameters)

    for line in output.split('\n')[start:end]:
        if(line == ""):
            continue
        date = str(datetime.datetime.strptime(line.split()[0], "%Y-%m-%d"))
        outputjson['SpendingItems'].append({
                                        'Date': date,
                                        'Amount': sanatize(line.split()[1])
                                     })

    return HttpResponse(
        simplejson.dumps(outputjson),
        content_type = 'application/javascript; charset=utf8'
    )

def recentactivity(request, account):
    parameters = ['-w', '-F', '%(date)\t%(payee)\t%(amount)\t%(total)\t%(note)\n', '-d', 'd>[today]-180', '-c', '--sort', 'd', 'reg', account]
    return activity(request, parameters, True)

def forecastactivity(request, account):
    parameters = ['-w', '-F', '%(date)\t%(payee)\t%(amount)\t%(total)\t%(note)\n', '--forecast', 'd<=[today]+365', '-d', 'd>[today] & d<[today]+365', '--sort', 'd', 'reg', "^" + account]
    return activity(request, parameters, False)

def activity(request, parameters, reverse):
    query = request.GET
    limit = int(query.get('limit', 10000))
    start = int(query.get('start', 0))

    output = runledger(parameters)
    output = output.split('\n')
    if(reverse):
        output.reverse()

    if(limit):
        end = start + limit
    else:
        end = None

    outputjson = {}
    outputjson['success'] = True
    outputjson['total'] = len(output) - 1
    outputjson['ActivityItems'] = []

    for line in output[start:end]:
        if(line == ""):
            continue
        line = line.replace('$', '').replace(',','').replace('%','')
        date, payee, amount, balance, notes = line.split('\t', 4)
        amount = sanatize(amount)
        balance = sanatize(balance)
        outputjson['ActivityItems'].append({
                                        'Date': date,
                                        'Payee': payee,
                                        'Amount': amount,
                                        'Balance': balance,
                                        'Notes': notes,
                                     })
    if(reverse):
        outputjson['ActivityItems'].reverse()

    return HttpResponse(
        simplejson.dumps(outputjson),
        content_type = 'application/javascript; charset=utf8'
    )


def forecast(request, account):
    base_parameters = ['--sort', 'd', '--daily', '-J']
    parameters = base_parameters + ['--forecast', 'd>[today] & d<[today]+365', '-d', 'd>[today] & d<[today]+365', 'register', "^" + account]
    return balance(request, parameters)


def eomestimate(request):
    #End of month estimate calculation
    balance = float(sanatize(runledger(['-F', '%(amount)', '-c', 'bal', 'FirstTech:Checking'])))
    budget_list = sanatize(runledger(['-c', '-p', 'this month', '-F', '%(total)', '--flat', '--budget', '-M', 'bal', '^exp', 'liab', 'invest']))
    budget = float(budget_list.split()[-1])

    income = sanatize(runledger(['--forecast', 'd>[today] & d<[next month]', '-p', 'from this day', '-J', 'reg', 'salary']))
    if(len(income.split())):
        income = float(income.split()[1])*-1
    else:
        income = 0.0

    if(budget < 0):
        eom = balance + income + budget
    else:
        eom = balance + income

    outputjson = {}
    outputjson['success'] = True
    outputjson['eom'] = eom

    return HttpResponse(
        simplejson.dumps(outputjson),
        content_type = 'application/javascript; charset=utf8'
    )




def assets(request):
    parameters = ['-c', '--no-total', '--flat', 'balance', 'assets' ]

    query = request.GET
    limit = int(query.get('limit', 10000))
    start = int(query.get('start', 0))

    output = runledger(parameters)

    if(limit):
        end = start + limit
    else:
        end = None

    outputjson = {}
    outputjson['success'] = True
    outputjson['assets'] = []

    for line in output.split('\n')[start:end]:
        if(line == ""): continue
        line = line.replace('$', '').replace(',','').replace('%','')
        amount, account = line.split(None, 1)
        outputjson['assets'].append({
                                        'Amount': amount,
                                        'Account': prettyname(account)
                                     })

    return HttpResponse(
        simplejson.dumps(outputjson),
        content_type = 'application/javascript; charset=utf8'
    )


def accounts(request):
    parameters = ['accounts' ]

    query = request.GET
    limit = int(query.get('limit', 10000))
    start = int(query.get('start', 0))

    output = runledger(parameters)

    if(limit):
        end = start + limit
    else:
        end = None

    outputjson = {}
    outputjson['success'] = True
    outputjson['accounts'] = []
    outputjson['total'] = len(output.split('\n')) - 1

    for line in output.split('\n')[start:end]:
        if(line == ""): continue
        outputjson['accounts'].append({
                                        'Name': line
                                     })

    return HttpResponse(
        simplejson.dumps(outputjson),
        content_type = 'application/javascript; charset=utf8'
    )


